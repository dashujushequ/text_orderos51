# C语言标准库实现的非实时抢占多任务

#### 介绍
C语言标准库实现的非实时抢占多任务，非常适合51系列单片机和内存有限的系统上运行，用C语言标准库实现，代码总共243行数，移植方便

目前完成了任务的增删改。

1，增
可以增加任意一个全局无参无返回值的函数作为新任务，但是新增任务名必须已经定义的全局函数，否则程序可能奔溃

2、删
支持任务名删除现有的任务，支持任务编号删除现有的任务

3、改
支持修改任务优先级，支持修改任务名，但是任务名必须已经定义的全局函数，否则程序可能奔溃